// Introduction to Node.js

// require() is a directive to load Node.js modules
// http module let us transfer data using http (Hyper Text Transfer Protocol)
// module software component that contains one or more routines (methods)
const http = require("http");

console.log(http);

// createServer() method - it creates an HTTP server that let us listen to a specified port and gives responses back to the client
// shorthand for request, response - req, res
http.createServer((request, response) => {

	// writeHead method - sets status code for the response
	// sets the content-type of the response
	response.writeHead(200, {'Content-Type' : 'text/plain'})

	// end method - sends the response with text content
	response.end('Hello World')

}).listen(4000);

console.log('Server is running at port 4000');
console.log('Hi I am here');